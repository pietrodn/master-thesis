#!/bin/bash

spellcheck () {
    LANG=$1
    DIR=$2

    for i in $DIR/*.tex; do
        echo $i
        aspell check -l $LANG -t --encoding=utf-8 \
            --add-tex-command="autocite p" \
            --add-tex-command="autoref p" \
            --add-tex-command="texttt p" \
            --add-tex-command="begin pp" \
            $i
    done
}

spellcheck it_IT content-it
spellcheck en_EN content-en
