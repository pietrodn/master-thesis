\chapter[Introduzione]{Introduzione{\footnote{Questo ampio estratto in lingua italiana ottempera all'obbligo previsto dall'art.\ 4.2 del ``Regolamento d’Ateneo degli Esami di Laurea e di Laurea Magistrale con integrazioni della Scuola di Ingegneria Industriale e dell'Informazione'', approvato dal Senato Accademico del 23.1.2017 e dalla Giunta della Scuola del 9.2.2017.}}}
\label{cha:introduction-ita}

\section*{Ambito}
% [contextualize the domain]

L'analisi statica dei file eseguibili --- ossia l'analisi del codice eseguibile eseguita senza tracciarne l'esecuzione --- è un approccio efficace per estrarre informazioni dai programmi eseguibili quando il codice sorgente non è disponibile.
L'analisi statica dei binari fa parte del dominio più ampio dell'ingegneria inversa dei programmi eseguibili.
Due degli scopi più importanti dell'analisi statica sono: ottenere un corretto \emph{disassembly} delle istruzioni macchina, e costruire un corretto \emph{Control Flow Diagram} (CFG) del programma.

L'analisi statica dei binari è possibile solo se alcune informazioni sul file eseguibile da analizzare sono note in precedenza: per poter disassemblare un eseguibile, è necessario conoscere l'ISA (\emph{Instruction Set Architecture}) del binario e i confini della sezione del file che contiene il codice eseguibile.
La conoscenza dell'ISA è necessaria perché l'ISA definisce la codifica binaria delle istruzioni macchina: la stessa sequenza di byte, infatti, può avere due significati totalmente diversi per due CPU diverse.
Conoscere i confini delle regioni contenenti il codice è necessario per assicurarsi che il disassembler lavori sul codice, e non sugli altri dati contenuti all'interno del file eseguibile.

Le informazioni sull'ISA e sulle sezioni del file eseguibile si trovano di solito nell'intestazione dell'eseguibile stesso.
In alcuni casi, tuttavia, tale intestazione è assente: questo è il caso di programmi eseguiti direttamente sull'hardware di un dispositivo (firmware).
Inoltre, i file eseguibili possono essere deliberatamente offuscati per ostacolare i tentativi di ingegneria inversa: questo è il caso dei \emph{malware}.
In tutti questi casi dobbiamo estrarre le informazioni necessarie analizzando il file eseguibile, senza disporre dei relativi metadati.

Anche quando l'ISA ed i confini delle regioni di codice sono noti, il problema di ottenere un corretto \emph{disassembly} dell'eseguibile resta comunque non banale poiché il \emph{disassembler} rischia di interpretare erroneamente come delle istruzioni i dati inseriti all'interno della sezione codice da parte di alcuni compilatori.
Infatti, le intestazioni dei formati eseguibili standard (ad esempio ELF e PE) non includono le informazioni sulla posizione dei dati inclusi all'interno della sezione di codice, a meno che non siano disponibili gli appositi simboli di debug.
Per la maggior parte dei programmi (soprattutto commerciali e \emph{off-the-shelf}), i simboli di debug non sono disponibili, perché faciliterebbero l'ingegneria inversa del prodotto.


\section*{Definizione del problema e obiettivi}
% [explain and motivate what is the problem with concrete numbers and objective evidence (summary of and hooks to Section 2)]

In questo lavoro proponiamo tre nuovi approcci per risolvere tre problemi aperti nel campo dell'analisi statica dei programmi eseguibili.
I tre problemi di cui ci occuperemo sono:

\begin{enumerate}
    \item \textbf{Identificazione dell'\emph{Instruction Set Architecture}}: dedurre l'architettura della CPU di un file eseguibile privo di intestazione;
    \item \textbf{Identificazione della sezione codice}: determinare dove sia collocata la sezione eseguibile, contenente il codice macchina, all'interno di un file eseguibile privo di intestazione;
    \item \textbf{Problema della separazione tra codice e dati}: all'interno della sezione eseguibile di un file binario, distinguere accuratamente (a livello di byte) tra le istruzioni macchina e i dati \emph{inline}.
\end{enumerate}

Daremo una spiegazione approfondita di questi tre problemi e delle sfide che li accompagnano nel~\autoref{cha:motivation}.

\section*{Stato dell'arte}
% [1 phrase to describe the top 1-2 state-of-the-art approaches that solve the problem (summary of and hooks to Section 2b)]
% [explain the top 1-2 reasons why the above 1-2 approaches fall short (hooks to Section 2b)]

Nella~\autoref{sec:soa}, presenteremo una rassegna approfondita dello stato dell'arte nella letteratura scientifica relativamente ai problemi sopra descritti.

Il problema della classificazione dei file in base al loro tipo è stato ampiamente affrontato in letteratura (\autoref{ss:soa-file-classif}), ma solo un lavoro è specificamente orientato all'identificazione dell'architettura dei file eseguibili: l'approccio descritto in~\autocite{clemens_2015} consiste in un modello di apprendimento supervisionato che usa le frequenze dei byte come feature per classificare i file binari in base alla loro architettura.
Riteniamo che i risultati di questo lavoro possano essere migliorati introducendo feature aggiuntive ed ottimizzando i parametri degli algoritmi di apprendimento.

Il problema dell'identificazione del tipo dei segmenti di file --- affrontato ad esempio in~\autocite{sportiello_2012} --- è simile al problema dell'identificazione della sezione contenente il codice negli eseguibili; tuttavia, nel nostro caso la frammentazione non è definita ``a priori'': necessitiamo di un metodo che segmenti il file e, contemporaneamente, classifichi i segmenti.

Le \emph{Conditional Random Field}~\autocite{lafferty_2001} sono dei modelli di apprendimento supervisionato e strutturato in grado di predire un insieme di variabili le cui dipendenze condizionali sono codificate in un grafo.
Le applicazioni delle CRF spaziano nei campi del riconoscimento del linguaggio naturale, della \emph{computer vision} e della bioinformatica.
In~\autocite{rosenblum_2008}, un modello basato su CRF è usato per identificare le funzioni nel codice binario.

Un problema classico nel campo dell'analisi di binari è quello di distinguere il codice dai dati all'interno dei file eseguibili (la~\autoref{sec:soa} contiene una rassegna degli approcci esistenti).
I \emph{disassembler} esistenti producono risultati erronei qualora i dati si trovino inframmezzati con il codice macchina (analizziamo questo problema nella \autoref{ss:disassembler-performance}).
Gli approcci ``classici'' si basano sull'\emph{disassembly} ricorsivo (ossia, contrassegnare come codice tutti e soli gli indirizzi raggiungibili dall'esecuzione del programma), e tentano di migliorarne i risultati impiegando diverse euristiche, e l'esecuzione simbolica; nella~\autoref{ss:motivation-recursive-disassembly} descriviamo tre approcci di questo tipo.
La principale sfida legata a questo tipo di approcci è l'identificazione precisa delle destinazioni dei salti indiretti.~\autocite[4]{angr_2016}

Un approccio alternativo al problema della separazione tra codice e dati è quello di sviluppare un modello di apprendimento supervisionato sul codice macchina (descriviamo questa famiglia di approcci nella~\autoref{ss:statistical-methods}).
\autocite{wartell_2011} introduce un modello di apprendimento sequenziale (\emph{Prediction by Partial Matching}) per segmentare il codice x86 in istruzioni valide e dati.
Il modello ottiene un'accuratezza elevata, classificando correttamente come codice o dati più del 99.8\% dei byte.
La valutazione del modello è eseguita confrontando manualmente l'output del modello con il \emph{disassembly} generato da IDA Pro, poiché non è disponibile un disassembly ``perfetto'' dei binari a cui far riferimento; questa limitazione non permette di testare il modello su un numero elevato di binari.
Questo approccio supporta una sola architettura (x86), e si basa su alcune euristiche specifiche per quell'architettura: adattare il metodo per supportarne di nuove richiederebbe uno sforzo non trascurabile.

\section*{Approccio}
% [explain in brief what is your proposed approach (summary of and hooks to Section 3)]

Nel~\autoref{cha:approach}, descriviamo l'approccio che abbiamo sviluppato per risolvere i problemi sopraelencati.

Per risolvere il problema della classificazione degli eseguibili per architettura, miglioriamo l'approccio descritto in~\autocite{clemens_2015} costruendo un classificatore basato sulla regressione logistica con feature aggiuntive (le frequenze dei pattern relativi ai prologhi e agli epiloghi delle funzioni); aggiungiamo poi la regolarizzazione $L_1$ (Lasso) per migliorare le prestazioni del classificatore e la sua robustezza rispetto al rumore.
Il metodo è descritto nel dettaglio nella~\autoref{ss:approach-arch-classifier}.

Affrontiamo il problema dell'identificazione della sezione di codice e della separazione tra codice e dati costruendo un modello di apprendimento sequenziale basato sulle \emph{Conditional Random Field} (\autoref{sss:model-crf}).
Il nostro modello esamina la sequenza dei byte nei file binari e apprende, per ciascun byte, la probabilità che esso rappresenti del codice o dei dati, condizionata rispetto ai valori delle \emph{feature} e alla classe assegnata ai byte adiacenti.
Il modello, una volta addestrato sui binari del \emph{training set}, è in grado di dire se ciascun byte del binario da analizzare rappresenta del codice o dei dati.
Lo scopo del metodo di identificazione della sezione di codice (\autoref{ss:approach-code-section}) è quello di trovare dei segmenti di codice (e di dati) contigui e di grandi dimensioni all'interno del file, mentre lo scopo del metodo di separazione tra codice e dati (\autoref{ss:approach-code-discovery}) è quello di identificare precisamente ciascuna istruzione macchina.
Per questo motivo il modello per l'identificazione della sezione di codice include anche una fase di \emph{postprocessing} (\autoref{sss:approach-postprocessing}) per identificare i piccoli segmenti di codice o dati rilevati dal modello, che vengono interpretati come rumore e quindi scartati.

\section*{Implementazione}
% [explain in brief what how/if the approach is implemented (summary of and hooks to Section 4)]

Nel~\autoref{cha:implementation}, descriviamo l'implementazione del nostro approccio.
Abbiamo implementato tutti i metodi di apprendimento usando Python 3 e la libreria di \emph{machine learning} scikit-learn, ad eccezione di alcune operazioni di \emph{preprocessing} che sono state eseguite tramite script per Bash e PowerShell.

Il classificatore di architetture --- che oltre al modello vero e proprio comprende anche le fasi di preprocessing, di regolazione degli iperparametri e di validazione del modello --- è implementato tramite il framework di \emph{machine learning} per Python scikit-learn (\autoref{sec:impl-archclassifier}).

Per implementare i modelli di apprendimento sequenziale per l'identificazione della sezione di codice e la separazione tra codice e dati, utilizziamo \texttt{pystruct}, un framework di apprendimento strutturato per Python, che implementa le \emph{Conditional Random Field}, apprese tramite SVM strutturali (l'approccio è descritto nel dettaglio in~\autoref{sec:general-crf}).

Abbiamo implementato le fasi di \emph{preprocessing} e di apprendimento all'interno di classi Python.
Le nostre classi sono dei \emph{wrapper} che incapsulano i modelli generali di classificazione forniti dalle librerie di \emph{machine learning}.
In questo modo, esponiamo un'interfaccia semplice e uniforme nascondendo la complessità non necessaria di tali modelli.

Per implementare il nostro approccio al problema della separazione tra codice e dati, necessitiamo dei dati di \emph{training}, ossia un insieme di eseguibili per i quali siano note le posizioni delle istruzioni macchina e dei dati.
In letteratura, questo problema viene spesso affrontato utilizzando un \emph{disassembler} --- che non sempre forniscono un risultato corretto --- per fornire i dati di \emph{training}.
Noi scegliamo invece di estrarre i dati di \emph{training} dai simboli di debug generati durante la compilazione degli eseguibili, che sono più affidabili.
Questo approccio presenta alcune difficoltà tecniche, che affrontiamo nella~\autoref{sec:impl-code-discovery}.

\section*{Risultati}
% [explain what results were obtained in the experimental validation (summary of and hooks to Section 5)]

Nel~\autoref{cha:experimental-validation}, descriviamo la metodologia di valutazione dei metodi proposti in precedenza, e discutiamo i risultati degli esperimenti.

Per verificare i nostri metodi, utilizziamo l'\emph{holdout testing} e la cross-va\-li\-da\-zio\-ne, che sono tecniche standard e  consolidate nell'ambito del \emph{machine learning} (\autoref{sec:experimental-setup}).

Abbiamo valutato estesamente il classificatore di architetture su più dataset di file eseguibili; i risultati mostrano che il nostro metodo ottiene performance migliori dello stato dell'arte~\autocite{clemens_2015} su eseguibili ottenuti da articoli scientifici e software reali; ottiene prestazioni soddisfacenti anche sugli \emph{shellcode} e su binari sottoposti a \emph{packing} (\autoref{sec:experiments-arch-classifier}).
Il nostro modello è robusto rispetto al rumore: i risultati mostrano che esso è in grado di identificare correttamente l'architettura di file eseguibili contenenti sia sezioni con codice sia sezioni con dati.

Abbiamo valutato il metodo di identificazione delle sezioni di codice su quattro dataset contenenti file binari in tre formati (ELF, PE e Mach-O) e di più architetture hardware, compilati con diversi compilatori e livelli di ottimizzazione (\autoref{sec:experiments-code-section}). I risultati mostrano che il nostro modello riporta sempre ottime prestazioni: la percentuale media di byte correttamente classificati supera il 99,5\%.

Abbiamo valutato il metodo per la separazione tra codice e dati su un dataset di file binari per Windows (architetture x86 e x86-64) compilati con i simboli di debug.
I risultati mostrano come il nostro metodo raggiunga un'elevata accuratezza (oltre il 99,9\% di byte classificati correttamente) su questo dataset.

Abbiamo determinato i valori ottimali per gli iperparametri da utilizzare nei modelli in modo sistematico (\autoref{sec:experiments-hyperparam}).

\section*{Contributi originali}
% [conclude with a bullet list that summarizes what are the 3-4 contributions of the thesis over the state of the art]

I principali contributi del nostro lavoro di tesi sono i seguenti:

\begin{enumerate}
    \item un metodo di apprendimento supervisionato, basato su un semplice modello lineare, per identificare l'architettura hardware e l'endianness dei file eseguibili (che possono contenere sia codice che dati);
    \item un metodo di apprendimento sequenziale per identificare le sezioni di codice all'interno dei file eseguibili;
    \item un metodo di apprendimento sequenziale per separare i byte che rappresentano istruzioni macchina dai dati inseriti all'interno della sezione di codice di un eseguibile.
\end{enumerate}

\section*{Organizzazione della tesi}

Questa tesi è divisa in otto capitoli.

\begin{itemize}
    \item Nel~\autoref{cha:introduction}, forniamo un'ampia panoramica sui problemi che intendiamo affrontare, sugli obiettivi e sull'approccio proposto.
    \item Nel~\autoref{cha:motivation}, forniamo la definizione dei problemi, gli obiettivi della nostra tesi e le sfide da affrontare.
    \item Nel~\autoref{cha:approach}, descriviamo in dettaglio gli approcci, gli algoritmi e i modelli che abbiamo scelto di utilizzare per risolvere i problemi descritti nel~\autoref{cha:motivation}.
    \item Nel~\autoref{cha:implementation}, spieghiamo come abbiamo implementato gli approcci descritti nel~\autoref{cha:approach} in un prodotto software concreto.
    \item Nel~\autoref{cha:experimental-validation}, verifichiamo i nostri metodi su dati reali e diamo un'interpretazione dei risultati.
    \item Nel~\autoref{cha:limitations}, spieghiamo le limitazioni del nostro approccio.
    \item Nel~\autoref{cha:future-works}, proponiamo alcune direzioni per eventuali sviluppi futuri basati su questo lavoro.
    \item Nel~\autoref{cha:conclusions}, riassumiamo il lavoro fatto finora e tracciamo le conclusioni.
\end{itemize}
