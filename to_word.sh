#!/bin/bash

tex2docx() {
    mkdir -p $2
    for i in $1/*.tex; do
        pandoc $i -o $2/$(basename $i).docx
    done
}

tex2docx content-en docx/en
tex2docx content-it docx/it
