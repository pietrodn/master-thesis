\chapter{Implementation}
\label{cha:implementation}

In this chapter, we will explain how we implemented the approaches described in~\autoref{cha:approach} into concrete software tools.
In the first section, we will list all the tools that we used; then, we will describe in detail the implementation of our methods.

\section{Tools}

We used the following software tools and libraries to implement our methods:

\begin{itemize}
    \item Python 3.6.1 was used as the main programming language to write all our models; the following packages were also used:
    \begin{itemize}
        \item Scikit-learn 0.19.0 for the machine learning tasks (except the CRFs), feature transformation and model evaluation;
        \item NumPy 1.13.1 for efficient operations with vectors
        \item SciPy 0.19.1 for the sparse matrix operations;
        \item filebytes 0.9.13 for reading ELF, PE and Mach-O headers;
        \item python-magic 0.4.13 to determine the type of files;
        \item jupyter 1.0.0 for interactive data visualization;
        \item matplotlib 2.0.2 for graph plotting;
        \item pandas 0.20.3 to preprocess, load and store data;
        \item pystruct 0.2.4 for the machine learning tasks involving Conditional Random Fields (CRFs).
    \end{itemize}
    \item bash 3.2.57 for some of the data collection and preprocessing tasks;
    \item binutils 2.28 to extract information from ELF files and manipulate them;
    \item UPX 3.94 to pack executable files;
    \item macOS 10.12.5 as the primary OS to run all the experiments;
    \item VirtualBox 5.1.22 as the Virtual Machine Manager to run other operating systems as needed;
    \item Windows 10 (Evaluation version), inside a VirtualBox virtual machine, to compile the Windows binaries and to extract information from the debugging symbols;
    \begin{itemize}
        \item Visual Studio 2017 was used to build the Windows programs;
        \item PowerShell was used to automate the build process and the extraction of data from the symbol files;
    \end{itemize}
    \item Debian Linux 8 (``jessie'') was used inside a VirtualBox VM to download the Debian binaries to populate our dataset.
\end{itemize}

\section{Architecture classifier}
\label{sec:impl-archclassifier}

In this section, we will explain how we implemented the architecture classifier described in~\autoref{ss:approach-arch-classifier}.

\subsection{Dataset collection}

In this section, we will describe the tools that we developed to collect the datasets to train and test our CPU architecture classifier. The composition and the characteristics of each dataset are detailed in~\autoref{sec:dataset}.

The datasets come from different sources:
\begin{itemize}
    \item package repositories of Linux distributions;
    \item datasets from research papers;
    \item manually collected executables;
    \item datasets derived from the transformation of other datasets.
\end{itemize}

We developed two Bash scripts to automate the download and the unpacking of the software archives from the Debian Linux distribution.
For each architecture, and for each package, our script attempts to locate and download the corresponding archive. If this step is successful, the script extracts the archive and saves all the executable files in a separate location; all the other files are discarded.

We wrote two Bash scripts to obtain new datasets by transforming existing ones:
\begin{itemize}
    \item a script to extract the code section from a dataset containing full binary files;
    \item a script to pack all the executables in a dataset with the UPX packer.
\end{itemize}

We wrote a Bash script calling \texttt{arduino-builder}\footnote{\url{https://github.com/arduino/arduino-builder}} to compile the Arduino samples~\autocite{arduino_examples} to acquire more samples for the \texttt{avr} architecture.

\subsection{Preprocessing}

In this section, we will explain how we implemented the feature extraction phase to prepare the data for our classifier.

We implemented the preprocessing step in a method of the Python class of the learner model. The preprocessing step extracts the following features from a sample:

\begin{enumerate}
    \item the frequency of each byte;
    \item the frequency of each multi-byte pattern;
    \item the label (i.e., the true architecture of the binary).
\end{enumerate}

The multi-byte patterns are encoded into regular expressions.
Regular expression matching is done by the Python Standard Library \texttt{re} module.
To improve the performance, the regular expressions are pre-compiled beforehand using the \texttt{re.compile()} method.

To process an entire dataset, we developed a Python script (\texttt{dataset\-\_loader.py}) which spawns a pool of workers in multiple processes by using the \texttt{multi\-processing} module of the Python Standard Library. The number of workers is a parameter of the script. The workers execute the preprocessing step on the files in parallel, using multiple CPU cores.

The features extracted from the executable files are stored into a CSV file (one row per sample).
CSV is not the most compact format for storing this kind of information; however, we choose to use it because it is a \emph{de facto} standard: data in the CSV format can be inspected, loaded and converted by a multitude of software tools.

The preprocessing step is done once, before the model training phase: it would be too slow to recompute all the features on the fly whenever the model needs to be trained.
Also, once the preprocessing phase is completed, we do not need to have the original binaries at hand to train the classifier; only the file with the features needs to be distributed.

\subsection{Model training}
\label{ss:impl-archclassifier-training}

In this section, we will describe the implementation of the classification model.

We choose to use the Python machine learning framework Scikit-learn to implement our model. Scikit-learn is a well-known, established machine learning framework which offers many off-the-shelf supervised and unsupervised machine learning methods, as well as model evaluation facilities (cross validation, holdout, random sampling, computation of common performance metrics) and preprocessing functions. It also supports, for some of the models, parallel execution on multiple CPU cores.
Scikit-learn relies on NumPy for numeric computation. NumPy is a Python library, implemented in C, which offers fast, vectorized operations on matrices and vectors.

We implement our classification model in a class (\texttt{Binary\-Classifier}), written in Python, which internally uses the Scikit-learn's \texttt{Logistic\-Regression} classifier.
By implementing a wrapper class for the learner, we isolate all the code related to the model in the same class and hide all the unnecessary complexity of the Scikit-learn \texttt{Logistic\-Regression} class, which is a general-purpose model and exposes a multitude of options and parameters.
The \texttt{Logistic\-Regression} already supports one-vs-the-rest models, so we do not have to implement it from scratch.

We follow the Scikit-learn classifier interface~\autocite{sklearn-api} for our class to be able to use the model evaluation features of Scikit-learn.
In particular, we implement the following methods in our learner class:

\begin{itemize}
    \item \texttt{fit(X, y)}: fit the model on the features in $X$ and on the true labels in $y$;
    \item \texttt{predict(X)}: returns the predicted class for each of the samples represented by the features in $X$;
    \item \texttt{predict\_proba(X)}: similar to \texttt{predict(X)}, but instead of returning a single prediction for each sample, it returns a vector containing the probabilities for that sample to belong to each class.
\end{itemize}

Our wrapper class also implements the methods to preprocess a single sample, and to save and load the trained model.

Our learner is initialized with the following parameters:
\begin{itemize}
    \item \emph{penalty}: whether to use L1 (lasso) or L2 (ridge) regularization for the Logistic Regression;
    \item \emph{C}: the inverse of the regularization strength (if $C$ is smaller, more regularization is applied);
    \item \emph{num\_jobs}: how many parallel jobs to use for model training.
\end{itemize}

To determine the optimal value for the regularization strength parameter, we use the \texttt{GridSearchCV} class of Scikit-learn.
\texttt{GridSearchCV} performs an exhaustive search in the parameter space, evaluating the model (with cross-validation) on each combination of the parameter values.
The user specifies the domain of each parameter.

In our case, we only optimize for the regularization strength ($C$), in the experiment described in~\autoref{sec:arch-hyperparam}.

\subsection{Model evaluation}

We wrote a Python script to evaluate the model on each dataset, and to store into a JSON file the performance measures, the information about the dataset and the parameters of the evaluated model.
We compute the cross-validation prediction for each sample by using the \texttt{cross\_val\_predict} function in Scikit-learn.

We describe our evaluation methodology in greater detail in~\autoref{sec:experimental-setup}.

\section{General sequential learning model}
\label{sec:general-crf}

In this section, we explain how we developed a sequential learning classifier to implement the code section identification and the code discovery methods.

Both the problems of code section identification and code discovery can be stated as sequence learning and prediction problems; the only things that change are: how an element in the sequence is defined, and which features are used to describe each element.

The classifier we implemented is based on linear-chain Conditional Random Fields, described in~\autoref{ss:approach-code-section}.
We decided to use the models provided by \texttt{pystruct}~\autocite{mueller_2014}, an open-source and ready-to-use structured learning library implementing CRF-like models with Structural SVMs learners (SSVMs).
We chose this framework because of the quality of the documentation, the simplicity of use, and the good classification performance.

We wrap the general-purpose CRF model of \texttt{pystruct} into a Python class (\texttt{CRFModel}) to hide the complexity of the general-purpose models in \texttt{pystruct}, to expose a clear and simple API to the end user, and to ensure the compatibility with the Scikit-learn APIs.
Our class wraps the \texttt{ChainCRF} and the \texttt{FrankWolfeSSVM} classes of \texttt{pystruct}, which provide the core learning algorithms; in addition, we implement our postprocessing algorithm (described in~\autoref{sss:approach-postprocessing}).

\autoref{tab:crf-input} shows the format of the input data of our model.
Each sample (i.e., sequence) $i$ is represented by a feature matrix ($X_i$) containing the one-hot encoded features for each element of the sequence, as described in our approach (\autoref{sss:part-2-preprocessing}); the ground truth for a training sample (containing a label for each element in the sequence) is represented by the ground truth vector $y_i$.

\begin{table}
\begin{center}
    \begin{tabular}{|c|c|c|cccccc|}
    \cline{3-9}
    \multicolumn{2}{c|}{} & \multirow{2}{*}{$\mathbf{y_i}$} & \multicolumn{6}{c|}{$\mathbf{X_i}$} \\
    \cline{4-9}
    \multicolumn{2}{c|}{} & & \texttt{0x01} & \texttt{0x02} & \texttt{0x03} & $\cdots$ & \texttt{0xFE} & \texttt{0xFF} \\
    \hline
    \multirow{7}{*}{\rotatebox[origin=c]{90}{\textbf{Address}}}
    & \texttt{0x0001} & 0 & 0    & 1    & 0    & $\cdots$ & 0    & 0    \\
    & \texttt{0x0002} & 0 & 1    & 0    & 0    & $\cdots$ & 0    & 0    \\
    & \texttt{0x0003} & 1 & 0    & 0    & 1    & $\cdots$ & 0    & 0    \\
    & \texttt{0x0004} & 1 & 0    & 0    & 0    & $\cdots$ & 0    & 1    \\
    & \vdots & \vdots & \vdots & \vdots & \vdots & $\ddots$ & \vdots & \vdots \\
    & \texttt{0xFFFE} & 1 & 0    & 1    & 0    & $\cdots$ & 0    & 0    \\
    & \texttt{0xFFFF} & 0 & 0    & 0    & 0    & $\cdots$ & 1    & 0    \\
    \hline
    \end{tabular}
    \caption[Exemplification of the input format for our sequential learning model]{Exemplification of the input format for our sequential learning model. $X_i$ is the feature matrix for the sample $i$ (a binary file, i.e., a sequence of bytes to classify), containing the one-hot encoded features (columns) for each byte (row) to classify; $y_i$ is the ground truth vector for the sample $i$, containing one label for each element.}
    \label{tab:crf-input}
\end{center}
\end{table}

For performance reasons (explained in~\autoref{ss:impl-code-section-preprocess}) our model needs to work with sparse feature matrices.
The current version of \texttt{pystruct} does not support sparse matrices as an input for its models; however, it is easy to overcome this problem by applying a simple patch in the code that has already been proposed as a pull request\footnote{\url{https://github.com/pystruct/pystruct/pull/4}} on GitHub.

The interface of \texttt{CRFModel} (our class) follows the Scikit-learn classification API and can be used with the model evaluation functions.
\texttt{CRFModel} implements the following methods:

\begin{itemize}
    \item \texttt{fit(X, y)}: fit the model on the list of feature matrices $X$ and the list of ground truth vectors (true labels) $y$;
    \item \texttt{predict(X)}: return, for each of the samples in $X$, the vector of predictions;
    \item \texttt{score(X, y)}: called on a fitted model, computes the average accuracy for the samples;
    \item \texttt{save(filename)}: saves the trained model into a file;
    \item \texttt{load(filename)}: loads the trained model from a file.
\end{itemize}

Our classifier takes the following initialization parameters:
\begin{itemize}
    \item \texttt{C}: the regularization strength as defined by the \texttt{FrankWolfeSSVM} learner of \texttt{pystruct};
    \item \texttt{max\_iter}: the maximum number of iterations of the learner;
    \item \texttt{lookahead} and \texttt{lookbehind}: how many lookahead or lookbehind bytes to include in the feature matrix.
\end{itemize}

Our learner automatically generates the lookahead and the lookbehind features (\autoref{sss:part-2-preprocessing}) in the \texttt{fit()} and \texttt{predict()} methods. The lookahead and lookbehind features for all the elements in the sequence are generated by vertically shifting a copy of the feature matrix and joining it (horizontally) with the original one.
All these operations are performed efficiently, without leaving the sparse matrix format.

Our code section identification method also includes the postprocessing phase (\autoref{sss:approach-postprocessing}), to be applied to the results of the prediction of the CRF model. We implemented the postprocessing algorithm by subclassing our \texttt{CRFModel} and overriding the \texttt{predict} method.
The subclass (\texttt{CRF\-Postprocess\-Model}) takes three additional initialization parameters:

\begin{enumerate}
    \item \texttt{post\_process}: whether to activate the postprocessing step or not;
    \item \texttt{postprocessing\_cutoff} and \texttt{postprocessing\_min\_sections}: the parameters to be given to Algorithm~\ref{alg:postprocess};
\end{enumerate}

The class diagram of our model is shown in~\autoref{fig:class-crf}.

\begin{figure}
    \includegraphics[width=\textwidth]{media/CRFClasses}
    \caption{Class diagram of our CRF models and of the pystruct API.}
    \label{fig:class-crf}
\end{figure}


\section{Code section identification}
\label{sec:impl-code-section-id}

In this section, we will explain how we implemented the code section identification method described in~\autoref{ss:approach-code-section}.
The core learning algorithm is the same described before, in~\autoref{sec:general-crf}.

\subsection{Preprocessing}
\label{ss:impl-code-section-preprocess}

In this subsection, we will explain how we extract the features from the binary files in the dataset.

The initial dataset for the code section identification task consists in a collection of ELF, PE or Mach-O executables, all belonging to the same CPU architecture.
As explained in~\autoref{ss:background-executable-file-formats}, each executable file is divided into ``sections,'' containing either executable code or data; the sections are listed in the file header.
The header of the executable file also specifies, for each section, whether it contains executable code or not.

We recall that the purpose of this analysis is to identify the boundaries of the executable sections.
We see the binary file as a sequence of bytes, and we want to classify each byte either as belonging to an executable section, or to a non-executable section. We associate a set of \emph{features} to each byte of the executable file.
As described in the approach (\autoref{ss:approach-code-section}), for each byte we use its one-hot encoded value (0--255) as a feature.

By one-hot encoding the value of each byte, for a binary file of $N$ bytes we produce a feature matrix of $(N \times 256)$ elements.
Each element of the feature matrix is an unsigned 8-bit integer, which is the smallest data type supported by NumPy. A representation encoding each element of the matrix in a single bit would be 8 times more space-efficient, but we did not find a practical way to implement it within the NumPy stack.
This representation of the features introduces a performance problem: if we had 100 executables of size 1 MB each in our dataset, we would be using 25.6 GB of main memory for the feature matrices!

These feature matrices are, by their nature, \emph{sparse}: since the bytes are one-hot encoded, each row of the matrix contains a single ``one'' and 254 zeroes.
Thus, we can save a significant amount of memory by using a compressed representation of the feature matrix.
We use the \textbf{CSR (Compressed Sparse Row)} sparse matrix representation~\autocite{buluc_2009}, which is natively implemented by SciPy.
To keep the memory usage as low as possible at all times and to avoid spikes in the memory usage, we generate the feature matrix from the original file directly in the CSR format, without passing from the ``dense,'' standard matrix representation.

More formally, SciPy's CSR matrix creation function requires three vectors:
\begin{enumerate}
    \item $\mathit{data}$: $\mathit{data}(k)$ is the value of the $k$-th non-zero element in the matrix, in row-major order;
    \item $\mathit{rows}$: $\mathit{rows}(k)$ is the row coordinate of the $k$-th non-zero element;
    \item $\mathit{cols}$: $\mathit{cols}(k)$ is the column coordinate of the $k$-th non-zero element.
\end{enumerate}

The relationship between the above vectors and the original matrix $A$ is the following:

\begin{equation}
    A(i, j) =
    \begin{cases}
        data(k) & \text{if} \; \exists \, k \; \text{s.t.} \; i=rows(k) \land j=cols(k)\\
        0 & \text{elsewhere}
    \end{cases}
\end{equation}

We initialize the CSR matrix by specifying the three vectors as follows: the $\mathit{data}$ vector is filled with ones; the $\mathit{rows}$ vector is the sequence of integer numbers ranging from $1$ to $N$ (the number of bytes); the $\mathit{cols}$ vector is the list of the integer values $v_i$ of the bytes in the file ($v_i \in [0, 255]$).

In the preprocessing phase, we do not generate the \emph{lookahead} or \emph{lookbehind} features: those are generated internally by the common sequential learning models described in~\autoref{sec:general-crf}.

Finally, we need to extract the ground truth from the executable files in the training set: this is easy because the ELF, PE and Mach-O headers provide the information about which sections of the binary file are executable.
First, we use the Python module \texttt{python-magic} which uses the standard UNIX interface (the so-called ``magic file''\footnote{\url{https://linux.die.net/man/5/magic}}) to determine the file type of the sample (ELF, PE or Mach-O).
To parse the header information, we use \texttt{filebytes}~\autocite{filebytes-github}, which is a library written in Python capable of parsing the ELF, PE, and Mach-O headers.
The ground truth generation method tags as code (``1'') all the bytes belonging to any executable section which (according to the file header), and tags as data (``0'') all the remaining bytes in the file. The final result is a binary vector of the same length of the executable file.

To summarize, the final output of the preprocessing phase, for each sample $i$ of length $N$ bytes, is:
\begin{itemize}
    \item the $(N \times 256)$ feature matrix $X_i$, containing the one-encoded byte values of the file;
    \item the ground truth vector $y_i$, of length $N$, telling whether each byte belongs to an executable section or not.
\end{itemize}

\subsection{Model training}
\label{ss:impl-codedata-training}

For the code section identification problem, we train the \texttt{CRFPostprocessModel} illustrated in~\autoref{fig:class-crf} and described in~\autoref{sec:general-crf}.
The model is fitted with the data generated from the preprocessing phase; the executables to analyze follow the same pipeline.

To determine the optimal values for the lookahead and lookbehind lengths, we use the \texttt{GridSearchCV} class of Scikit-learn to perform an exhaustive search over the parameter space.

The number of iterations of the Frank-Wolfe block-coordinate SSVM learner also needs to be tuned. In general, more iterations lead to a better accuracy.
We manually tuned the maximum number of iterations to obtain a fair tradeoff between the training time and the accuracy of the model.

\subsection{Model evaluation}
\label{ss:impl-codedata-eval}

To visualize the results of our method, we developed a Jupyter notebook which uses \texttt{matplotlib} to compare the ground truth data and the prediction output of our model, by means of a ``barcode plot'' (\autoref{fig:code-section-visualization-example}).
We mark with two different colors the bytes corresponding to code and data, and we provide the results before and after the postprocessing phase.

\begin{figure}
    \includegraphics[width=\textwidth]{media/CodeSectionVisualizationExample}
    \caption[Visualization of the results of the code section identification method on a binary file]{Visualization of the results of the code section identification method on a binary file. From top to bottom: the ground truth, the prediction of our model before postprocessing, the prediction errors, the prediction of our model after postprocessing, and the prediction errors after the postprocessing. Precision and recall are computed on the bytes, considering ``code'' as the positive class.}
    \label{fig:code-section-visualization-example}
\end{figure}

We developed a script to evaluate the code section identification model over multiple datasets and save the results in the JSON format.
The script uses the model evaluation functions in Scikit-learn.
The evaluation methodology is described in detail in~\autoref{sec:experimental-setup}.

\section{Code discovery}
\label{sec:impl-code-discovery}

The purpose of the code discovery method is to predict, within the code section of an executable file, which bytes correspond to valid CPU instructions and which bytes are inline data inserted by the compiler between CPU instructions.
In this section, we will explain how we implemented the code discovery approach described in~\autoref{ss:approach-code-discovery}.

\subsection{Preprocessing (Windows x86)}

In this section, we will explain how we implemented the automated collection of a dataset for the code discovery method, how we extract the ground truth for the debugging symbols, and how we generate the features for our model.

\subsubsection{Dataset collection}

The choice of a dataset for our supervised code discovery model is constrained by the need for precise ground truth, i.e., the knowledge about which bytes correspond to valid, executable machine code and which others do not.
Specifically, we want to extract such ground truth from the symbol files generated during the compilation process.
So, we have to compile from source some Windows programs, and to configure the compiler to generate the symbol files.
To do this, we need a set of programs to compile and a configured compilation environment.

Microsoft makes available to developers the \emph{Universal Windows Platform (UWP) app samples}~\autocite{microsoft-samples}, a set of code samples demonstrating the usage of the Windows APIs. These samples are shipped as ready-to-use Visual Studio projects (``solutions''), so they are a convenient choice for our dataset.

Microsoft makes available to developers a Windows 10 Virtual Machine, with Visual Studio 2017 already pre-installed~\autocite{microsoft-vs-vm}. We start from this system to configure our compilation environment.
MSBuild~\autocite{microsoft-msbuild} --- the official Microsoft command-line build automation tool --- allows to compile a Visual Studio project with the correct options without interaction, i.e., without having to manually open the project in Visual Studio.

We were not able to automate the build of the C\# projects with MSBuild, so we resorted to compile all the C++ projects available in the samples.
C++ compilation requires the installation of additional packages in Visual Studio: this is easily done by opening a C++ project in Visual Studio and following the instructions.

We faced another problem with MSBuild: even by specifying the option \texttt{/p:DebugType=full}, the resulting debug symbol file lacked the private symbols, i.e., the function information which provides the ground truth.
From a verbose run of MSBuild, we identified the problem: MSBuild does not pass the correct flag to the linker. As a workaround, we replaced all the occurrences of \texttt{DebugFastLink} with \texttt{DebugFull} in the file:

\begin{lstlisting}
C:\Program Files (x86)\Microsoft Visual Studio\2017\Community\Common7\IDE\VC\VCTargets\Microsoft.Link.Common.props
\end{lstlisting}

which is one of the configuration files of the linker. In this way, the linker always generates full (public and private) debug symbols.

We wrote a simple PowerShell script to iterate the execution of MSBuild over all the C++ code samples. PowerShell is a shell scripting language designed by Microsoft to provide a more powerful interface than the traditional DOS prompt language.
After figuring out the correct combination of options to provide to MSBuild, we compiled the samples for both the x86 and the x86-64 architectures.

After the compilation process, we ran a script to collect all the \texttt{.exe}, \texttt{.sys} and \texttt{.dll} files together with their associated symbol files (PDB files), and move them to a separate location.

\subsubsection{Features and ground truth extraction}
To parse the PDB files, we used the \texttt{dia2dump}~\autocite{microsoft-dia2dump} tool by Microsoft. \texttt{dia2dump} uses Microsoft's APIs to parse a PDB file and dumps all the information into a plain text file.
The sources of \texttt{dia2dump} (C++) need to be compiled in Visual Studio, for the x86-64 architecture, with the ``Debug'' configuration.
We discovered that compiling it with the ``Release'' configuration causes the program to crash at runtime.
To run \texttt{dia2dump} without errors, we also had to register a DLL provided by Microsoft in the DIA SDK, by using the command:

\begin{lstlisting}
\windows\syswow64\regsvr32.exe "C:\Program Files (x86)\Microsoft Visual Studio\2017\Community\DIA SDK\bin\amd64\msdia140.dll"
\end{lstlisting}

After completing these steps, the \texttt{dia2dump} executable is ready to run.
We developed another PowerShell script to automate the execution of \texttt{dia2dump} on all the PDB files in our dataset and save the output into plain text files.

Finally, we convert the textual representation of each PDB file into the final ground truth format, i.e., a binary vector telling, for each byte in the executable file, whether it corresponds to a valid machine instruction or to data.
We implemented this step in a Python program, following Algorithm~\ref{alg:code-data} in~\autoref{ss:approach-code-discovery}.

We extract the byte-level features from the executable files exactly as we did for the code section identification method (\autoref{ss:impl-code-section-preprocess}).
For each executable file, we only generate the features and the ground truth relative to the code section, discarding all the other sections of the file.

\subsection{Preprocessing (ARM)}

In this section, we will explain how we obtained the training set for an architecture with fixed instruction length (ARM), and how we extracted the features and the ground truth from the executable files.

We gathered the coreutils compiled for ARM\footnote{\url{https://github.com/BinaryAnalysisPlatform/arm-binaries}} with debugging symbols. The debugging symbols are embedded into each ELF executable, in the standard DWARF format~\autocite{eager_2012}.

We use the \texttt{objdump} linear disassembler from the GNU binutils library,\footnote{\url{https://sourceware.org/binutils/docs/binutils/objdump.html}} which is able to yield a perfect disassembly of these executables if the DWARF symbols are present.

To generate the features (\autoref{sss:approach-code-discovery-arm-preprocess}) from each file in the training set, we first strip the debugging symbols with the \texttt{strip} utility from the binutils, and then we run \texttt{objdump} on the stripped file. In this way, our model learns on the same features \texttt{objdump} would generate for the executable files to analyze, which miss the debugging symbols.

To extract the ground truth, we run \texttt{objdump} on the original ELF files, without stripping the DWARF symbols: in this way, the disassembler knows for certain which 4-byte words are instructions and which words are data. For the latter, the disassembler outputs the string \texttt{.word} in place of the opcode.

We implemented the ground truth and feature extraction phases in a Python class, which calls \texttt{objdump} on each file and parses the disassembly output.

\subsection{Model training and prediction}

The model training and prediction phases for the code discovery method are similar to the analogous phases for the code section identification method (\autoref{ss:impl-codedata-training}).

We train the \texttt{CRFModel} class (\autoref{fig:class-crf}), which implements the CRF sequential learning model described in~\autoref{sec:general-crf}.
Unlike the code section identification approach, this model does not perform any postprocessing on the prediction outputs.

Since we implemented the preprocessing and the learning phases in separate classes, we can use the same learner for both the Windows and the ARM datasets, even if the features used for classification are different.

To determine the optimal values for the lookahead and lookbehind lengths, we use the \texttt{GridSearchCV} class of Scikit-learn to perform an exhaustive search over the parameter space.
