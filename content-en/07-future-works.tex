\chapter{Future Works}
\label{cha:future-works}

In this chapter, we will discuss some possible future research directions, starting from the ideas described in this work.

\section{Architecture classifier}
\label{sec:future-archclass}

Our supervised learning approach to classify the CPU architecture of executable files may be extended by introducing an automated feature engineering phase, to automatically identify the most relevant byte patterns instead of relying on a fixed set of patterns, as we do in the current approach.
This improved feature engineering phase would identify the ``best'' patterns, i.e., those which discriminate the most between the architectures. These patterns could be either simple multi-byte sequences, or regular expressions.

This idea, however, would add significant complexity to the model, introducing additional problems.
Sequential pattern mining is computationally expensive, because the number of patterns to test increases exponentially with their length.
Moreover, such a model could overfit on features extraneous to the code (e.g., padding sequences or common data patterns).
In~\autocite{rosenblum_2008}, the authors performed automated pattern mining to generate the features for their function identification approach; the feature selection phase consumed over 150 compute-days (2 days in real time) in a highly parallel and distributed environment.

\section{Code section identification}
\label{sec:future-code-section}

Our code section identification approach could be used to solve the more general problem of type identification of file fragments, a recurring task in forensic analysis.
Also, our model could be applied to classify the sub-components of a compound file, i.e., a file which contains segments of heterogeneous types (e.g., a PDF file containing text, bitmap images, and vector graphics).

Our approach can also be used to find the executable sections inside firmware blobs. Unfortunately, it would not be easy to evaluate such an approach on firmwares because of the lack of ground truth data.

Our method could be further extended to identify other kinds of sections inside executable files (jump tables, headers, the GOT, relocations, \ldots).

\section{Code discovery}
\label{sec:future-code-data}

The quality and the coverage of the ground truth provided by the debugging symbols may be further improved.
As we explained in~\autoref{ss:approach-code-discovery}, the debugging symbols do not cover some portions of the binaries. In our experiments, we discarded those portions since we already had plenty of training data.

The information from the debugging symbols could be combined with those coming from a disassembler.
This hybrid approach would help to cover a wider portion of the code, and would provide other useful features to use in the model.
In~\autocite{andriesse_2016}, the authors follow an alternative approach: a linear disassembly is started at each byte corresponding to the beginning of every line of code. This approach does not give a \emph{complete} disassembly, because not every machine instruction has a corresponding line in the source code; however, it could be used in combination with other techniques (as the paper does).

The model we use (linear-chain CRF) is a flexible and general-purpose sequence classification model: in principle, any feature can be assigned to the items of the sequence. In our implementation, we only used the one-hot encoded byte values, but other features may be inserted, e.g., those resulting from a disassembler or from the knowledge of the opcodes of the ISA.\@

Our current model does not have any notion of ``instruction'': the Conditional Random Field simply labels each byte as ``code'' or ``data,'' without hard constraints over the classification results. This means that our approach could potentially tag as ``code'' some subsequences which are not valid instructions in the ISA.\@
This risk is balanced by the advantages of a simple approach: learning is fast and no external knowledge is needed.

A more advanced model could include the knowledge about the ISA, e.g., by allowing only valid instructions to be classified as code.
Introducing this constraint is not trivial: the model would need to perform structured learning on two different levels of abstraction, simultaneously: bytes and instructions.
The improved model would have to label each byte either as code or data, and to group the bytes corresponding to code into valid instructions.

In~\autocite{wartell_2011} the instruction set is taken into account, but the segmentation algorithm only considers byte-level features.
In~\autocite{karampatziakis_2010}, the constraints given by the instruction set are encoded in a graph structure in which each byte is a node.

A further step forward would be to learn also over the sequence of \emph{instructions} identified by the model itself, in the case of variable length instructions. New instruction-level features would be introduced (e.g., the opcodes).
We were able to assign features to the instructions only in the fixed-length case, where the instructions are trivially segmented in 4-byte words and can be directly labeled (\autoref{sss:approach-code-discovery-arm-preprocess}); the linear-chain CRF model is not powerful enough to assign features to subsequences of bytes in the variable-length instruction case.

Semi-CRFs~\autocite{sarawagi_2005} are an extension of Conditional Random Fields, which allow to assign features and labels to subsequences (i.e., instructions) of elements (i.e., bytes).
Training semi-CRFs is computationally more complex than ``plain'' CRFs; nevertheless, they may represent a valid approach to build a more advanced model to solve the code discovery problem in executable files.

Our code discovery approach for ARM binaries may be used to improve the results of the code discovery method for ARM/Thumb mixed ISA binaries~\autocite{chen_2013}, or extended to directly support Thumb (16-bit instructions) code.
