\chapter{Approach}
\label{cha:approach}

In this chapter, we will describe in detail the approaches we developed to solve the problems defined in~\autoref{cha:motivation}.
First, we will give a broad overview, and then we will discuss each approach in detail.

\section{Approach Overview}
\label{sec:approach-overview}

The \textbf{architecture classification problem} consists in predicting the CPU architecture and the endianness of an executable file without relying on any header information.
To solve this problem, we follow an approach similar to~\autocite{clemens_2015}, which faces the problem as a machine learning classification task. We assume not to have any metadata about the executables to classify.

We collect a training set of binaries (``samples'') compiled for multiple architectures, from which we extract the byte-level features, i.e., the frequencies of occurrence of single bytes and of selected multi-byte patterns.
We then train a logistic regression model to predict the ISA and the endianness of unseen binaries from these features.

\begin{figure}
    \includegraphics[width=\textwidth]{media/ArchitectureClassifier.pdf}
    \caption{Diagram of the logical steps of the architecture classification method.}
    \label{fig:approach-arch-classifier}
\end{figure}

The logical steps of the architecture identification approach, illustrated in~\autoref{fig:approach-arch-classifier}, are the following:

\begin{enumerate}
    \item \textbf{Data collection}
    \item \textbf{Feature engineering and preprocessing}
    \item \textbf{Training}
    \item \textbf{Prediction}
\end{enumerate}

The \textbf{code section identification problem} consists in predicting whether each byte in an executable file belongs to an executable section or not.
Our approach works as follows.
We collect a training set of labeled binaries of the same architecture of the executables that we want to analyze; then we train a probabilistic sequence learning model (linear-chain CRF) on such training set.
The trained model can identify the code-to-data and the data-to-code transitions in any unlabeled executable.
The sequential learner classifies each byte as code or data: as we want to identify one single, contiguous, code section inside the binary file, we develop a postprocessing step to eliminate small blocks of code or data, by merging them with the surrounding region.
As we will show in~\autoref{ss:experiment-code-section-debian}, the postprocessing phase improves the performance of our model by eliminating the noise in the prediction output.

\autoref{fig:approach-code-section-pre} describes the data collection and preprocessing steps; \autoref{fig:approach-code-section-learning} describes the model learning, prediction and postprocessing steps.

The logical steps of the code section identification approach are the following:
\begin{enumerate}
    \item \textbf{Data collection}
    \item \textbf{Preprocessing}
    \item \textbf{Learning the model}
    \item \textbf{Prediction}
    \item \textbf{Postprocessing}
\end{enumerate}

The \textbf{code discovery problem} consists in predicting whether each byte inside the code section of an executable file belongs to a valid CPU instruction, or should be considered inline data.
We solve this problem with the same model used to solve the code section identification problem.
First, we collect a training set of executables in which the code/data tagging is known, by compiling some programs with full debug information. We train a probabilistic sequence learning model (linear-chain CRF) on such training set. The trained model will predict, for an unknown binary, which bytes belong to a valid CPU instruction and which ones are to be considered as data.

The logical steps of the code discovery approach are the following:
\begin{enumerate}
    \item \textbf{Data collection and preprocessing}
    \item \textbf{Training}
    \item \textbf{Prediction}
\end{enumerate}

\autoref{fig:approach-inline-data-pre} illustrates the data collection and preprocessing phases; \autoref{fig:approach-inline-data-learning} illustrates the model learning and prediction phases.

\begin{figure}
    \begin{subfigure}{\textwidth}
        \includegraphics[width=\textwidth]{media/CodeSectionPreprocessingApproach.pdf}
        \caption{Preprocessing steps to generate the training dataset.}
        \label{fig:approach-code-section-pre}
    \end{subfigure}
    \par\vspace{4em}
    \begin{subfigure}{\textwidth}
        \includegraphics[width=\textwidth]{media/CodeSectionLearning.pdf}
        \caption{Model learning and prediction phases.}
        \label{fig:approach-code-section-learning}
    \end{subfigure}
    \caption{Diagrams for the code section identification approach.}
\end{figure}

\begin{figure}
    \begin{subfigure}{\textwidth}
        \includegraphics[width=\textwidth]{media/CodeDataPreprocessingApproach.pdf}
        \caption{Steps for the generation of the dataset.}
        \label{fig:approach-inline-data-pre}
    \end{subfigure}
    \par\vspace{4em}
    \begin{subfigure}{\textwidth}
        \includegraphics[width=\textwidth]{media/CodeDataLearning.pdf}
        \caption{Model learning and prediction phases.}
        \label{fig:approach-inline-data-learning}
    \end{subfigure}
    \caption{Diagrams for the code discovery approach.}
\end{figure}

\section{Architecture classification}
\label{ss:approach-arch-classifier}

\subsection{Preprocessing and feature engineering}

We train our model on a set of executable files, labeled with the correct CPU architecture.
The executables can be in any format (e.g., PE or ELF), as we do not rely on any header information.

Since we assume to have no metadata about the executable files (no disassembly is possible at this time), the only choice is to extract the features from the bytes in the files.

We obtain the \emph{Byte Frequency Distribution} (BFD) of each file by computing the frequencies of the 256 possible bytes.
The frequency of a byte having integer value $i$ is defined as:

\begin{equation}
    f_i = \frac{count(i)}{\sum_{i=0}^{255} count(i)} \qquad \forall i \in [0, 255]
\end{equation}

where $count(i)$ is the number of times that the byte $i$ appears in the executable.
We choose the BFD as the initial feature set for our model since it is a common and effective choice in literature (\autoref{ss:soa-clemens}).
The underlying assumption which supports the choice of the BFD as a set of features is that executables compiled for different architectures have different BFDs.

In~\autoref{fig:bfd-bison}, we show a graphical representation of the Byte Frequency Distribution computed for the same program, compiled for four CPU architectures.
The differences between the three diagrams are evident, and can be spotted even by manual inspection.
Two architectures (mips and mipsel) share the same BFD diagram, which we reported only once in~\autoref{fig:bfd-bison-mips}. The reason these two architectures have an identical BFD is that they differ only by their endianness.

\begin{figure}
    \begin{subfigure}[b]{\linewidth}
        \includegraphics[width=\textwidth]{media/bfd_bison_amd64}
        \caption{bison (amd64)}
        \label{fig:bfd-bison-amd64}
    \end{subfigure}

    \begin{subfigure}[b]{\linewidth}
        \includegraphics[width=\textwidth]{media/bfd_bison_mips}
        \caption{bison (mips, mipsel)}
        \label{fig:bfd-bison-mips}
    \end{subfigure}

    \begin{subfigure}[b]{\linewidth}
        \includegraphics[width=\textwidth]{media/bfd_bison_armel}
        \caption{bison (armel)}
        \label{fig:bfd-bison-mipsel}
    \end{subfigure}

    \caption[Byte Frequency Distributions of the same executable compiled for 4 architectures]{Byte Frequency Distributions of the \texttt{bison} executable compiled for four different architectures (amd64, mips, mipsel, armel). Note that the BFDs for the \texttt{mips} and \texttt{mipsel} architectures are identical, since they differ only for the endianness which is not reflected into the BFD.}
    \label{fig:bfd-bison}
\end{figure}

The second set of features is conceptually similar to the BFD: it consists in the frequencies of some specific byte patterns, which are more frequently found in some architectures than in others.
These multi-byte patterns are encoded as regular expressions, which in our opinion represent a fair tradeoff between expressive power and matching speed.\footnote{Regular expressions can recognize regular languages, a task which requires linear time w.r.t. the length of the input string.~\autocite[91]{reghizzi_2013}}
The choice of the patterns to include in this set of features fell on the patterns of function prologues and epilogues, available in the angr's \texttt{archinfo} project~\autocite{angr_archinfo}.

We included in this set of features the four two-byte patterns used in~\autocite{clemens_2015}, which help to recognize the endianness of the binary, i.e., the byte bi-grams \texttt{0x0001}, \texttt{0x0100}, \texttt{0xfffe}, \texttt{0xfeff}.
A model using only the Byte Frequency Distribution (i.e., the frequency of the single bytes) would be unable to distinguish two binaries of the same CPU architecture differing only by their endianness, because the two files would differ only in the ordering of the bytes, not in their frequency.

The number of matches of these patterns are normalized by the length of the file (number of bytes), to account for executable files of different sizes. Thus, the multi-byte-pattern features are computed as follows:

\begin{equation}
    freq(pattern) = \frac{matches(pattern, file)}{len(file)}
\end{equation}

In the preprocessing phase, each file is transformed into a numeric vector containing the values of the features.
The preprocessing phase can be parallelized over the files.

\subsection{Training}

In this section, we describe the classification model which learns the parameters of the model from the training set and, once trained, can predict the class (architecture) of any unknown sample.

We choose to use \textbf{Logistic Regression} (LR) with L1 regularization (lasso regularization), a linear classification model which learns a vector of parameters $w$ by solving the following minimization problem over the feature matrix $X$ and the vector of labels $y$:

\begin{equation}
    \label{eqn:logistic-l1}
    \min_{w, c} \left\| w \right\|_{1} + C \sum_{i=1}^{n} \log \left( \exp \left( - y_i \left( X_i^T w + c \right) \right) + 1 \right)
\end{equation}

The cost function minimized by~\autoref{eqn:logistic-l1} is composed of two terms: the first is the regularization term, which assigns a penalty to ``complex'' models with large coefficients; the second term is the logistic loss.
The $C$ parameter is the inverse of the regularization strength: higher values of $C$ assign more importance to the second term in~\autoref{eqn:logistic-l1}, while lower values of $C$ give more importance to the regularization term.
Regularization is a technique used in machine learning to avoid \emph{overfitting}, i.e., to learn models which are accurate on the training set but generalize poorly on unseen samples.
We choose L1 regularization because Lasso can generate compact models, by setting to zero the coefficients in $w$ corresponding to less relevant features. In this way, our model performs feature selection as part of the learning phase.
For a more thorough explanation of the Logistic Regression model and of the usefulness of regularization in classification models, we refer to~\autocite{bishop_2006}.

Logistic Regression, as described in~\autoref{eqn:logistic-l1}, is only able to discriminate between two classes. We obtain a multi-class classifier by using the \emph{one-vs-the-rest} (OvR) strategy.
The OvR strategy consists in fitting one classifier for each class $c \in C$, to distinguish the samples of $c$ (``positive'' class) against all the other classes $C \setminus \{c\}$ (considered together as the ``negative'' class).

Each Logistic Regression classifier outputs a \emph{confidence score} for each sample, i.e., an estimate of the probability that the sample belongs to the positive class.
The class label predicted for a sample is the class with the highest confidence score returned by the corresponding classifier.

We chose the Logistic Regression (LR) model for the following reasons:
\begin{itemize}
    \item LR is a simple, linear model, less prone to overfitting than more complex models;
    \item the only hyperparameter to tune is the regularization strength;
    \item LR is a popular model for classification tasks and it is available as an off-the-shelf component from several machine learning libraries;
    \item training and prediction are fast;
    \item the model is compact: it can be represented with $(\text{num\_features} \, \times \, \text{num\_classes})$ parameters;
    \item when classifying an unknown sample, LR not only returns the predicted class, but also a confidence score for each class;
    \item L1-regularization can produce a compact model, by putting to zero the weights of less important features;
    \item the model weights can give an estimate of the relative importance of the features;
    \item LR is one of the best-performing models in~\autocite{clemens_2015}.
\end{itemize}


\subsection{Prediction}

After fitting the model with the training data, it can predict the CPU architecture of any unseen executable.
The preprocessing phase for the binaries to analyze is the same used for the training set.

For any sample $\mathbf{x}$ to predict, the model returns, for each class $C$, a confidence score: this score can be interpreted as the probability $p(C|\mathbf{x})$ that the sample belongs to that class.
The class predicted by the model is:

\begin{equation}
C^{*} = \argmax_C p(C|\mathbf{x})
\end{equation}


\section{Code section identification}
\label{ss:approach-code-section}

\subsection{Data collection}

The training dataset consists in a collection of ELF, PE or Mach-O executable files, whose CPU architecture and location of the executable section are known.
These files, according to their specifications, are divided in \emph{sections}. Each section has a different purpose: there are sections containing executable code, static data, debug information and metadata, etc.

The executable file header contains information about the sections of the executable file: we are interested in the starting offset, the length, and the flag telling whether the section is executable or not.

\subsection{Preprocessing}
\label{sss:part-2-preprocessing}

Since we assume that the binary files to analyze can contain both code and data, we can not disassemble the executable to generate instruction-level features.
Instead, we derive the features directly from the byte sequence of the binary file.

For each byte in the file, we take the one-hot representation of its value (from 0 to 255) as a feature vector.
For example, for the byte $b_i=4$, the resulting 256-vector is: $x_i = (0, 0, 0, 0, 1, 0, 0, \ldots, 0)$.
This choice derives from the observation that the byte value itself has little meaning for the purposes of our approach, and any ordering among byte values would be meaningless~\autocite[8]{shin_2015}, because we are only interested in distinguishing one byte from the others. In other words, the byte values are treated as \emph{categorical features}.
One-hot encoding is a typical approach to deal with categorical features in machine learning.
One-hot encoding of the bytes turns each binary file in our dataset to a $(N \times 256)$ matrix of ones and zeros, where $N$ is the number of bytes in the file.

We extend the preprocessing phase to also consider, for each byte, the values of the $m$ preceding bytes (\emph{lookbehind}) and of the $n$ following bytes (\emph{lookahead}). $m$ and $n$ are parameters of the model and can be optimized by hyperparameter tuning.
These extended features are one-hot encoded as well, and appended to the vector corresponding to each byte.
So, from an executable file of $N$ bytes, we derive a matrix of $N \times (256 \cdot (n + m))$ elements.

The remaining part of the preprocessing phase consists in extracting the ground truth, i.e., the location information of the sections of the executable files.
The offset and the size of each section containing executable code are found in the file header: from this information, we derive the ground truth in the form of a binary vector (``mask''), of the same length of the number of bytes in the executable file.
In this vector, the ones correspond to the bytes belonging to executable sections, while the zeros correspond to the bytes belonging to non-executable sections.

For example, if we have a file of 16 bytes and the executable section spans from 6 to 13, the vector would be:

\[
    y = (0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0)
\]

Executable files may have more than one executable section: the ones in the vector correspond to all the bytes belonging to any section which is marked as ``executable'' in the file header.

\subsection{Learning the model}
\label{sss:model-crf}

We translate the code section identification problem to a classification problem over a sequence of bytes.

The problem can be formalized as follows: for each training sample, we have a sequence of observed bytes ($\mathbf{X}$) and a sequence of labels ($\mathbf{Y}$):
\begin{equation}
    \begin{aligned}
        \mathbf{X} &= (x_1, x_2, \ldots, x_n) & \qquad x_i &\in [0, 255] \\
        \mathbf{Y} &= (y_1, y_2, \ldots, y_n) & \qquad y_i &\in \{0, 1\}
    \end{aligned}
\end{equation}

We want to learn a model able to predict the label sequence $\mathbf{Y}$ corresponding to any unlabeled input sequence $\mathbf{X}$.

The model of choice is a linear-chain Conditional Random Field~\autocite{lafferty_2001}; the learning is performed by structural SVMs~\autocite{taskar_2004}.
All the explanation below follows the steps of the two works cited before.

Conditional Random Fields (CRFs) are probabilistic models to segment and label data structured over a graph. In the simple case of linear-chain CRFs, the graph reduces to an undirected sequence.
CRFs are a \emph{structured learning} model, i.e., they do not simply classify each item (byte) in the sequence separately, but consider the structure of the problem.
Classifying each byte separately would inevitably lead to a poor model, since in some architectures every byte can be interpreted as a part of a valid instruction, or as data: we must take the context into account.

In a structured learning setting, $\mathbf{X}$ is a set of random variables representing the sequence of observations, and $\mathbf{Y}$ is a set of random variables over the corresponding labels.
We assume that some variables in $\mathbf{Y}$ are conditionally dependent: the dependencies are encoded in a graph in which the vertices are the elements of $\mathbf{Y}$ and an edge between two nodes $\mathbf{Y}_v$ and $\mathbf{Y}_w$ represent a conditional dependence between the two variables $v$ and $w$.

CRFs can be seen as an extension of Hidden Markov Models, with a fundamental difference: while HMMs are generative models and model the joint probability $p(\mathbf{X}, \mathbf{Y})$, CRF do not model the distribution of $\mathbf{X}$ (which in general can be complex): instead, they model $p(\mathbf{X} \mid \mathbf{Y})$, i.e., the probability of $\mathbf{X}$ conditioned on $\mathbf{Y}$.
This allows to relax the independence assumptions on the observations, which are a limitation of Hidden Markov Models.

We report the definition of a Conditional Random Field from~\autocite[5]{lafferty_2001}:

\begin{quote}

Let $G = (V, E)$ be a graph such that $\mathbf{Y} = (\mathbf{Y}_v)_{v \in V}$, so that $\mathbf{Y}$ is indexed by the vertices of $G$.

Then $(\mathbf{X}, \mathbf{Y})$ is a \emph{conditional random field} in case, when conditioned on $\mathbf{X}$, the random variables $\mathbf{Y}_v$ obey the Markov property with respect to the graph: $p(\mathbf{Y}_v \mid \mathbf{X}, \mathbf{Y}_w, w \neq v) = p(\mathbf{Y}_v \mid \mathbf{X}, \mathbf{Y}_w, w \sim v)$, where $w \sim v$ means that $w$ and $v$ are neighbors in $G$.

\end{quote}

Put in simpler words, a CRF is a graph of random variables; each variable is conditionally dependent on all its neighbors, and conditionally independent from all the other variables.

\begin{figure}
    \begin{center}
        \includegraphics{media/CRFFigure.pdf}
        \caption[Graphical structure of a linear-chain CRF]{Graphical structure of a linear-chain CRF~\autocite[6]{lafferty_2001}.}
        \label{fig:crf-figure}
    \end{center}
\end{figure}

In the case of linear-chain CRFs (\autoref{fig:crf-figure}), the graph structure is an undirected linear sequence, meaning that the variable associated with each element ($\mathbf{Y}_v$) depends on the observations and on the classification of the previous ($\mathbf{Y}_{v-1}$) and the following ($\mathbf{Y}_{v+1}$) element.
The dependencies between the variables in $\mathbf{Y}$ obey the Markov property on the graph, i.e., each variable is conditionally independent from all non-neighbor variables.

To compute the conditional probabilities in each node, we define a set of \emph{feature functions}; they can be defined on the vertices, or on the edges:

\begin{equation}
    \begin{aligned}
        f_h(\mathbf{X}, \mathbf{Y}) &= \sum_{i \in V} f_h(\mathbf{X}, \mathbf{Y}_i) \\
        g_k(\mathbf{X}, \mathbf{Y}) &= \sum_{(i, j) \in E} g_k(\mathbf{X}, \mathbf{Y}_i, \mathbf{Y}_j)
    \end{aligned}
\end{equation}

In our approach, the feature function associated to each vertex $\mathbf{Y}_i$ is the one-hot encoded byte value $b_i$; the feature function associated to each edge is always constant and equal to 1.

Then, we associate unary and binary \emph{potential functions} $\phi$ respectively to each vertex $i$ and to each edge $(i, j)$ in $G$.
The Markov network model is log-linear, so we can compute the network potentials as the exponential of the weighted sum of the features on the vertices and on the edges:

\begin{equation}
    \begin{aligned}
        \phi_{i}(\mathbf{X}, \mathbf{Y}_i)
        &= \exp \left[ \sum_{h} w_h f_h(\mathbf{X}, \mathbf{Y}_i) \right]
        & \quad\forall \, i &\in V \\
        \phi_{i, j}(\mathbf{X}, \mathbf{Y}_i, \mathbf{Y}_j)
        &= \exp \left[ \sum_{k} w_k g_k(\mathbf{X}, \mathbf{Y}_i, \mathbf{Y}_j) \right] & \quad\forall \, (i, j) &\in E
    \end{aligned}
\end{equation}

where the weights $w_i$ are the parameters learned by the model.

Finally, we compute the conditional probability distributions as:

\begin{equation}
P(\mathbf{Y} \mid \mathbf{X}) \propto
\prod_{i \in V} \phi_{i}(\mathbf{X}, \mathbf{Y}_i)
\prod_{(i, j) \in E} \phi_{i, j}(\mathbf{X}, \mathbf{Y}_i, \mathbf{Y}_j)
\end{equation}

To learn the CRF model, we use Structural Support Vector Machines (SSVMs)~\autocite{taskar_2004}.
SSVMs are soft-margin SVMs~\autocite[325]{bishop_2006} with a loss function designed for multi-label classification.

We report the original primal problem formulation for soft-margin SSVMs from~\autocite[4]{taskar_2004}.

\begin{equation}
    \begin{aligned}
        \min \quad & \frac{1}{2} \left\| \mathbf{w} \right\|^2 + C \sum_{\mathbf{x}} \xi_{\mathbf{x}} & \\
        \text{s.t.} \quad & \mathbf{w}^\top \Delta \mathbf{f}_{\mathbf{x}}(\mathbf{y}) \ge \Delta \mathbf{t}_{\mathbf{x}}(\mathbf{y}) - \xi_{\mathbf{x}} &\quad \forall \, \mathbf{x}, \mathbf{y}
    \end{aligned}
\end{equation}

where:
\begin{itemize}
    \item $\mathbf{w}$ is the vector of weights learned by the model;
    \item $\mathbf{t}(\mathbf{x})$ is the predicted $\mathbf{y}$ for the input sequence $\mathbf{x}$;
    \item $\mathbf{f}(\mathbf{x}, \mathbf{y})$ are the \emph{features} or \emph{basis functions};
    \item $\Delta \mathbf{f}_{\mathbf{x}}(\mathbf{y}) = \mathbf{f}(\mathbf{x}, \mathbf{t}(\mathbf{x})) - \mathbf{f}(\mathbf{x}, \mathbf{y})$;
    \item $\Delta \mathbf{t}_{\mathbf{x}}(\mathbf{y}) = \sum_{i=1}^{l} I (\mathbf{y}_i \ne (\textbf{t}(\textbf{x}))_i)$ is the number of wrong labels predicted by the model for the input $\mathbf{x}$;
    \item $\xi_{\mathbf{x}}$ is a slack variable to allow the violation of some constraints when the data is not linearly separable;
    \item $C$ is the inverse of the regularization strength (the higher it is, the less the model is regularized).
\end{itemize}

To efficiently solve this optimization problem, we use the Block-Coordinate Frank-Wolfe algorithm for structural SVMs~\autocite{lacoste_2013}.
The Frank-Wolfe algorithm is an iterative optimization algorithm. Asymptotically, it converges to the solution; it can be stopped after a fixed number of iterations, or when the loss function becomes smaller than a threshold.

This ends the description of the general sequence learning model.

In our case, we train a linear-chain CRF model by using the training data obtained from the preprocessing phase.

\subsection{Prediction}

The fitted model can now be used to detect the location of the code section inside unseen binaries.
The features for the binary files to predict are generated with the same preprocessing steps used for the training set.

The output of the model is a prediction for each byte (``code'' or ``data'').
More formally, the model outputs a binary vector of the same format of the ground truth data, i.e., a sequence of ones and zeros of the same length of the input file. The ones correspond to the bytes predicted to be in an executable section, while the zeros correspond to the bytes predicted to be in a non-executable section.

\subsection{Postprocessing}
\label{sss:approach-postprocessing}

The code section identification model includes a postprocessing phase to improve the results given by the sequential classification model.
The objective of this phase is to ignore the small sequences of code inside the data sections (or vice versa), to return only few contiguous, large sequences of code or data.

The postprocessing phase is needed because, as explained in~\autoref{ss:problem-definition-code-discovery}, code sections may contain small pieces of data which are indistinguishable from the data found in data sections.

The postprocessing phase (Algorithm~\ref{alg:postprocess}) iteratively removes the smallest contiguous sequence of predictions (``chunk'') of code or data, merging it with the surrounding data or code (respectively).

The algorithm takes two parameters:
\begin{enumerate}
    \item $\mathit{MinSections}$: the minimum number of chunks to keep;
    \item $\mathit{Cutoff}$: the maximum size of the chunk that can be eliminated, as a fraction of the biggest chunk.
\end{enumerate}

Algorithm~\ref{alg:postprocess} always terminates: at each iteration, either a chunk can be eliminated and the loop iterates again, or no chunk can be eliminated and the procedure returns.

\begin{algorithm}
\caption{Postprocessing algorithm}
\label{alg:postprocess}
\begin{algorithmic}
    \REQUIRE $C$: list of chunks $(\text{start}, \text{end}, \text{tag})$, $\mathit{MinSections}$, $\mathit{Cutoff}$
    \LOOP
        \STATE $M \leftarrow \max_{c \in C} \len(c)$ \COMMENT{size of largest chunk}
        \STATE $\mathit{c_{min}} \leftarrow \argmin_{c \in C} \len(c)$ \COMMENT{smallest chunk}
        \IF{$|C| > \mathit{MinSections}$ \AND $\len(c_{min}) < \mathit{Cutoff} \cdot M$}
            \STATE invert tag of $c_{min}$ and merge with surrounding chunks
            \STATE $C \leftarrow \text{updated list of chunks}$
        \ELSE
            \RETURN{$C$}
        \ENDIF
    \ENDLOOP
\end{algorithmic}
\end{algorithm}

\section{Code discovery}
\label{ss:approach-code-discovery}

In this section, we explain our approach to solve the code discovery problem.
We developed two different approaches for the architectures with variable-length instructions (x86, x86-64) and for an architecture with fixed-length instructions (ARM).
The two approaches share the same sequential learning model; however, they use different features.

\subsection{Preprocessing (Windows x86)}
\label{sss:approach-code-discovery-win-preprocess}

The generation of a good dataset to train a learner to distinguish code from inline data is not a trivial task.
We already discussed the challenges that we have to face and the shortcomings of existing solutions in~\autoref{sec:challenges}.

The choice of the binary files to include in the training dataset is limited by some requirements:
\begin{enumerate}
    \item the executable must have a non-negligible amount of inline data inside the code section;
    \item the architecture must have variable-length instructions;
    \item ground truth information (i.e., whether each byte is code or data) must be available;
    \item ground truth information must be precise; for this reason, we do not want to rely on any disassembler.
\end{enumerate}

To our knowledge~\autocite{andriesse_2016}, the only compiler that inserts inline data in the code section of executables is Microsoft Visual Studio; this satisfies the first requirement.
To satisfy the second requirement, we consider the x86 and x86-64 architectures, which have variable-length instructions.
To satisfy the last two requirements, we choose to use the debugging symbols generated by the compiler to extract the ground truth.

Debugging symbols are generated by the compiler when the developer chooses to build a program with the debug configuration. Debugging symbols help developers to debug their programs, by providing a wealth of information about the structures found in the executable files: functions, variables, line number information, exported symbols, data types, and information about the original source files the program was compiled from. For instance, debug symbols allow to match machine instructions with source lines in the original source code.

Debugging symbols can be found in different formats, depending on the architecture, on the executable format and on the compiler support.
\autocite{kroustek_2012} describes which structures can be found in symbol files, and the technical difficulties involved in parsing them.

The symbols in ELF executables are usually stored in the \textbf{DWARF} format, which is well-documented~\autocite{eager_2012} and supported by static analysis tools and disassemblers.
DWARF symbols can be embedded in specific sections of the ELF, or they may be distributed in separate files.

On Windows platforms, the executables are shipped in the \textbf{PE (Portable Executable)} format, and the compiler (Visual Studio) exports the debug symbols into separate files in the \textbf{PDB (Program Database)} format, which is a proprietary format developed by Microsoft.
PDB is not standardized (``source code is the ultimate documentation''~\autocite{microsoft-pdb}) and all the tools needing the information contained in PDBs must rely on Microsoft's APIs, because the PDB file format is subject to change and there is no published standard to rely on.
The LLVM Project published a partial documentation about the information contained in PDB files~\autocite{llvm_pdb_format}.

Microsoft provides PDB files for the Windows system executables to help developers debug their code. The idea of including Windows system executables in the training set is attractive; however, the PDB files for the Microsoft binaries are \emph{stripped}~\autocite{microsoft-debugging} and do not contain \emph{private symbols}. This means that the information about the functions in the binary code is not available and that we can not extract the ground truth.
Indeed, the availability of private symbols would simplify the reverse-engineering of Microsoft's software, which is understandably not in the best interests of the company.
The impossibility of extracting ground truth from Windows binaries is also reported by~\autocite{bao_2014}.

Since we need full debug symbols (including function information) for each binary to extract the ground truth, and those symbols are not generally shipped with the executables, we have to compile some programs from source with Microsoft Visual Studio, enabling the generation of complete symbol files.

\texttt{dia2dump} is a tool provided by Microsoft~\autocite{microsoft-dia2dump} whose purpose is to parse a PDB file and to extract a textual representation of it.
By using \texttt{dia2dump} on the PDB files and by parsing, in turn, the output of \texttt{dia2dump}, we extract the starting and ending offset of each function in the executables, together with the locations of inline data.

We developed Algorithm~\ref{alg:code-data}, to tag each byte inside the code section of an executable as code or data starting from the information extracted from the PDB file.

\begin{algorithm}
\caption{Code/data tagging algorithm}
\label{alg:code-data}
\begin{algorithmic}
    \STATE $\mathit{tag}[i] \leftarrow DATA \quad \forall i$
    \FOR{every function $f$}
        \STATE $\mathit{tag}[\mathit{f.begin} : \mathit{f.end}] \leftarrow CODE$
        \FOR{$d \in \mathit{f.data}$}
            \STATE $\mathit{tag}[\mathit{d.addr} : \mathit{f.end}] \leftarrow DATA$
        \ENDFOR
    \ENDFOR
    \FOR{each thunk $t$}
        \STATE $\mathit{tag}[\mathit{t.begin} : \mathit{t.end}] \leftarrow CODE$
    \ENDFOR
    \RETURN $\mathit{tag}$
\end{algorithmic}
\end{algorithm}

The rationale for tagging all bytes as data by default is that the information contained in the PDB file does not cover all the bytes in the code section.
Indeed, between functions, there is a variable amount of padding bytes (\texttt{0xCC}), not reported in the PDB file.

The rationale for the data tagging step is that the PDB file reports the starting address of each data block, but not its length, which is variable.
By manual analysis of the binary files using IDA Pro, we found out that static data inside the functions is always located at the end of the function.

As suggested in~\autocite{bao_2014}, \emph{thunks} (short callbacks to addresses in the code) are tagged as code.

By manual inspection, we found out that in each binary there are some functions for which there is no information in the PDB file.
They are repetitive and short blocks of code; they appear to be exception handlers and polymorphic call handlers.
We suspect that the reason there is no function information for these blocks of binary code is that they do not correspond to any function in the original source code.
This code is always located in a specific sub-section of the \texttt{.text} section, identified as \texttt{.text\$x} in the PDB file.
Since we were unable to find further information about these code blocks, there is no ground truth for them, and they are isolated in a known sub-section of the file, we excluded these blocks from the training set.
This choice should not lead to any overestimation of the performance of our method, because the excluded code is very repetitive and would be easy to spot in an automated way.

Eventually, we obtain the training set, which consists in two files for each sample:
\begin{enumerate}
    \item \texttt{program.bin}: the sequence of byte we want to analyze (i.e., the contents of the \texttt{.text} section of the executable, after excluding the \texttt{.text\$x} sub-section);
    \item \texttt{program.mask}: a sequence of zeros and ones, of the same length of the \texttt{.bin} files, which tells whether each byte is code or data.
\end{enumerate}

\subsection{Preprocessing (ARM)}
\label{sss:approach-code-discovery-arm-preprocess}

ARM is a RISC architecture with fixed-length instructions. This means that every instruction and every data block starts at an address which is a multiple of 4 bytes.
The code discovery problem for architectures with fixed instruction length is simpler, because the instructions and the data are already segmented in fixed-size blocks.

The problem of code discovery for ARM executables can be stated as follows: to tell whether each 4-byte word in the executable section is an ARM machine code instruction, or data.

The ground truth for the training of the model is extracted from ARM executables compiled with debugging symbols.
Indeed, ARM debugging symbols contain mapping information which tells where code and data are located inside the executable sections~\autocite[24]{arm_elf_arch}.
We do not consider the case in which Thumb code (which is 2-byte aligned) is also present; that problem is addressed in~\autocite{chen_2013}.

To obtain the features from the binaries, we strip them (i.e., remove the symbols) and pass them to a linear disassembler.
The linear disassembler, if no mapping symbols are present, tries to interpret each word as an instruction and returns an opcode, or outputs the string \texttt{INVALID} if the 4 bytes can not be decoded into a valid ARM instruction.

We take as features, for each 4-byte word, \textbf{the opcode returned from the disassembler} and \textbf{the first byte of the word}.
These two features are one-hot encoded before being passed to the model.

The ground truth is simply obtained by passing the binaries, complete with the mapping symbols, to the linear disassembler. If the binaries are not stripped, the linear disassembler can use the symbols to identify inline data, returning a perfect disassembly of the executable file~\autocite{andriesse_blog_arm}.
The disassembler outputs the string \texttt{.word} whenever it encounters a word corresponding to data: from there, the ground truth is easily obtained.

\autoref{fig:arm-listing} shows an example of the differences in the output of \texttt{objdump} (the linear disassembler in the GNU binutils library) when the debugging symbols are missing.

\begin{figure}
    \centering
    \begin{subfigure}[b]{0.42\linewidth}
        \centering
\begin{lstlisting}[style=arm-asm-listing]
9214: add r3, pc, r3
9218: ldr r2, [r3, r2]
921c: cmp r2, #0
9220: bxeq lr
9224: b 9050
9228: .word 0x0000f1a8
922c: .word 0x00000118
\end{lstlisting}
        \caption{With symbols}
        \label{fig:arm-listing-symbols}
    \end{subfigure}
    \hspace{0.5cm}
    \begin{subfigure}[b]{0.48\linewidth}
        \centering
\begin{lstlisting}[style=arm-asm-listing]
9214: add r3, pc, r3
9218: ldr r2, [r3, r2]
921c: cmp r2, #0
9220: bxeq lr
9224: b 9050
9228: @andeq@ pc, r0, r8, lsr #3
922c: @andeq@ r0, r0, r8, lsl r1
\end{lstlisting}
        \caption{Without symbols}
        \label{fig:arm-listing-nosymbols}
    \end{subfigure}
    \caption[Disassembly of a portion of an ARM executable, with and without the debugging symbols]{Disassembly of a portion of an ARM executable, with and without the debugging symbols. In~\autoref{fig:arm-listing-symbols}, the disassembler correctly identifies the inline data because the debugging symbols are present. In~\autoref{fig:arm-listing-nosymbols}, the symbols are not present and the disassembler interprets the last two data words as instructions.}
    \label{fig:arm-listing}
\end{figure}

\subsection{Training and prediction}
\label{sss:approach-code-discovery-learning}

Starting from the features described in the previous sections, we generate the lookahead and the lookbehind features as explained in~\autoref{sss:part-2-preprocessing}.

We train a linear-chain Conditional Random Field (CRF)~\autocite{lafferty_2001}, which we described above in~\autoref{sss:model-crf} for the code section identification phase.

The executables to analyze are preprocessed in the same way of the training data (with the obvious exception of the ground truth extraction phase).
The trained model reads, as an input, the sequence of items (bytes or words) to classify as code or data; it gives back a sequence of code/data labels, one for each item (byte or word).

Differently from the code section identification approach, we do not apply the postprocessing phase since we are also interested in very small subsequences of code and data.
