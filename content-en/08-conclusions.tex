\chapter{Conclusions}
\label{cha:conclusions}

% [reprise of the goals stated in the Introduction]
In the Introduction (\autoref{cha:introduction}), we stated the following goals:

\begin{enumerate}
    \item develop a supervised learning method to identify the CPU architecture (ISA) and the endianness of header-less executable files;
    \item develop a sequential learning method to identify the boundaries of the code sections in a header-less binary file;
    \item develop a sequential learning method to solve the code discovery problem, i.e., distinguish the machine code from data inside the code section of stripped, header-less executable files.
\end{enumerate}

% [aposteriori summary of what you have done]
In~\autoref{cha:approach}, we presented the approach we chose to follow.

We faced the architecture identification problem as a supervised classification problem.
We collected the training sets for the classifier from multiple sources (packages from a Linux distribution, datasets from scientific papers, shellcodes, packed binaries).
We derived the features from the Byte Frequency Distribution and from the frequencies of multi-byte patterns representing function prologues and epilogues; developed the classifier using Logistic Regression with Lasso regularization; performed hyperparameter tuning to determine the optimal regularization strength.

To solve the code section identification problem, we developed a sequential learning model based on Conditional Random Fields learned via Structural SVMs.
We used the one-hot encoded byte values as features.
The model learns the conditional probabilities of the sequences of bytes in the code and data sections of the binaries in the training set.
We adapted the sequential learning library (\texttt{pystruct}) to support sparse feature matrices, to reduce the memory and CPU requirements of our method.
We developed a postprocessing algorithm, whose purpose is to reduce the noise in the prediction output, by coalescing the predictions for each byte into large, contiguous sections of the file.
Once it has been trained, our model can divide any binary file into code and data sections.
The strength of our approach, compared to the existing file fragment classification methods (\autoref{ss:soa-file-classif}), is that our model, by predicting the class of each byte, solves both the segmentation and the classification problems at the same time.

We developed a similar sequential learning approach to solve the code discovery problem, i.e., distinguish the bytes corresponding to machine code instructions from the bytes corresponding to inline data inside the executable section of a binary file.
We considered the most difficult case: a dataset of binaries for two architectures with variable-length instructions (x86 and x86-64).
We built our training set by compiling a set of Windows programs with full debugging symbols, which include the offsets of the functions and of the inline data; we extracted the ground truth by parsing the debugging symbols.
We trained the same sequential learning model described for the code section identification method (linear-chain CRF) on the training set, using the byte values as features.
The trained model predicts whether each byte of an unseen executable file is part of a valid machine instruction, or if it is inline data.

We also built a variant of this method to detect inline data inside binaries compiled for a fixed-instruction-length architecture (ARM).
This model is simpler and faster, because in ARM binaries the instructions and the data blocks are always aligned to 4-byte words inside the file.
We obtained the features by running a linear disassembler over the binary files and by collecting the opcodes decoded from each 4-byte word.
The DWARF debugging symbols inside the executables provide the ground truth.
We used the same CRF-based classifier as before; the only difference is that the sequential learner classifies 4-byte words instead of bytes.

% [aposteriori summary of the results]
In~\autoref{cha:experimental-validation}, we tested our methods on real data and evaluated the performance of our models.

We evaluated the architecture classifier on 20 different architectures; our model scored well with an averaged F-measure over 99\%.
The datasets include both files containing exclusively machine code, and complete binaries containing code and data sections.
We also tested our classifier on more challenging datasets (shellcodes and packed binaries): we still obtained a more than satisfactory performance.

We evaluated the code section identification method on binaries coming from: Debian packages for multiple architectures, a dataset of ELF and PE files from~\autocite{bao_2014}, and a macOS system. Our method scored well on all the datasets: on average, it classified correctly over 99.5\% of the bytes of the test samples.
The results of the experiments show that the postprocessing algorithm we developed consistently improves the performance of the model.
We determined the optimal values of the parameters of the model by hyperparameter tuning.

We evaluated the code discovery method on a dataset of Windows binaries compiled with full debugging symbols. The model correctly classified over 99.9\% of the bytes in the binaries.
We also tested the code discovery method on a dataset of ARM binaries. In this case, we obtained an accuracy of 99.98\%.
We determined the optimal values of the parameters of the model by hyperparameter tuning.

% [wrapup the limitations and future works]
An interesting development for the architecture classifier would be to add an automated feature engineering phase, i.e., an algorithm to autonomously generate and select the most relevant patterns for the classification task.
We decided not to implement such method, to keep our classifier simple and fast; our implementation, instead, relies on a fixed set of patterns.
The approach could be also extended to classify non-executable files by their types.
We do not expect our model to be resistant to deliberate obfuscation efforts specifically aimed to conceal the CPU architecture of executable files.

The code section identification method is not able to detect code inside compressed or encrypted files, and can be defeated by obfuscation techniques explicitly targeted to confuse our model (e.g., by putting large amounts of dead code in the data sections of the file).
Our approach may be extended to detect not only the executable regions of a binary files, but also other sections which exhibit peculiar patterns (e.g., jump tables, relocations, headers\ldots).
An approach similar to ours can be used, in general, to divide a file into segments and classify the segments by their type (this may be useful, for example, in the computer forensics domain, when file carving is not a suitable option).

The code discovery method can be improved by integrating the sequential learning model with a disassembler, e.g., by encoding the disassembler output into additional features.
Our model works at the byte level and has no knowledge of the notion of ``instruction'': the approach may be extended by introducing additional constraints --- derived from the knowledge of the ISA --- to enforce the consistency of the prediction output.

% [give your vision on the future]
Previous works (\autoref{ss:motivation-recursive-disassembly}) widely explored the strengths and the limitations of the static analysis approaches based on the recursive traversal of the code, and proposed increasingly sophisticated analyses over the instruction flow.
While some of these approaches have obtained remarkable results, they fall short whenever the execution flow cannot be statically traced with certainty (e.g., when complex indirect jumps are present).
The main limitation of this kind of approaches is that statically discovering all the reachable code regions in an executable file is an undecidable problem in computer science; the problem can not be resolved in the general case once and for all.

In this work, we address the problem of code/data separation by looking at the machine code from a purely syntactical, probabilistic point of view: our method consists in learning a language model to compute the likelihood of a byte to be code (or data).

We feel that the application of machine learning and statistical techniques to the static analysis of executable files will give substantial contributions in this research field.
