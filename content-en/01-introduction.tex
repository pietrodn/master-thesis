\chapter{Introduction}
\label{cha:introduction}

\section*{Domain}
% [contextualize the domain]

Static binary analysis (i.e., analyzing executable code without running it and tracing its execution) is an effective approach to extract information from executable files containing computer programs when the source code is not available.
Static binary analysis is part of the wider domain of binary reverse engineering.
Two of the most important purposes of static analysis are: obtaining a correct disassembly of the machine code and building a correct Control Flow Graph (CFG).

Static binary analysis is possible only if some information about the executable file to analyze is available.
The static analysis tools must at least know the \textbf{ISA (Instruction Set Architecture)} of the binary, and \textbf{the boundaries of the executable sections of the file} (i.e., the parts of the executable file which contain machine code).
The ISA defines the encoding of the CPU instructions of a program into a byte sequence: static analysis tools need this information to decode the machine code, because the same sequence of bytes may have two completely different meanings for two different CPUs.
Executable files are structured in sections, which include either code or non-executable bytes (``data''). Without the information about these sections, static analysis tools can not easily tell where the machine code is located inside the executable file.

The information about the ISA and about the sections inside the executable file is found in the header of the executable, if the file is provided in a standard format.
Sometimes, however, the header is missing, and we do not have any information at all about an executable file.
This is the case of the programs which run directly on a device's hardware (firmwares).
Also, executable files may be obfuscated to thwart the attempts of reverse engineering: this is the case of malware files.
In all these cases, we need to analyze the executable file without relying on the classical reverse engineering approaches.

Even if the ISA and the boundaries of the executable regions are known, the problem of obtaining a correct disassembly is still not trivial because some compilers insert \emph{inline data} inside the code sections, which may be incorrectly interpreted as machine code by the disassemblers.
Indeed, the header data found in the standard executable formats (ELF, PE, Mach-O, \ldots) does not include the information about the location of inline data inside the code section, unless the compiler is explicitly instructed to generate the debugging symbols. In most commercial and off-the-shelf programs, the debugging information is never included, because it would facilitate the reverse-engineering of the software product.


\section*{Problem definition and objectives}
% [explain and motivate what is the problem with concrete numbers and objective evidence (summary of and hooks to Section 2)]

In this thesis, we will propose three novel approaches to solve three open problems in the domain of static analysis of executable computer programs.
The three problems we will deal with are:

\begin{enumerate}
    \item \textbf{Instruction Set Architecture (ISA) identification}: inferring the CPU architecture of stripped, header-less executable binary files;
    \item \textbf{Code section identification}: determining where the executable section, containing machine code, is located inside stripped, header-less executable binary files;
    \item \textbf{Code discovery problem}: inside the executable section of a binary file, accurately distinguish (at the byte level) between valid machine code instructions and inline data.
\end{enumerate}

We will give a precise definition of these problems and describe the challenges associated with them in~\autoref{cha:motivation}.

\section*{State of the art}
% [1 phrase to describe the top 1-2 state-of-the-art approaches that solve the problem (summary of and hooks to Section 2b)]
% [explain the top 1-2 reasons why the above 1-2 approaches fall short (hooks to Section 2b)]

In~\autoref{sec:soa}, we will present a thorough literature review about the state of the art approaches to solve the problems mentioned before.

Generic file type classification is a widely explored problem in literature (\autoref{ss:soa-file-classif}); however, we found only one work specifically targeting the identification of the CPU architecture of executable files (\autoref{ss:soa-clemens}). This approach \autocite{clemens_2015} addresses the problem as a machine learning classification problem, by using the frequencies of the bytes in the files as features. We feel that the approach of this work can be improved by introducing additional features and by tuning the parameters of the machine learning algorithms.

The problem of the classification of file segments by type~\autocite{sportiello_2012} is similar to the problem of code section identification. The fundamental difference is that in our case we do not have any ``a priori'' file fragmentation: we must both segment the file and classify the segments.

Conditional Random Fields~\autocite{lafferty_2001} are a supervised, structured classification method to predict a set of variables whose conditional dependencies are encoded in a graph-like structure.
CRFs are popular in the domains of natural language processing, computer vision and bioinformatics.
In~\autocite{rosenblum_2008}, the authors build a CRF-based model to identify functions in binary code.

The problem of separating code and data inside executable files (``code discovery problem'') is a classical problem in static binary analysis.
Existing disassemblers fail to produce a correct disassembly when data is mixed together with machine code (\autoref{ss:disassembler-performance}).
The classical approaches rely on recursive disassembly (i.e., labeling as code all and only the locations reachable by the control flow of the program), and try to improve it by employing heuristics and symbolic execution; in~\autoref{ss:motivation-recursive-disassembly} we review three such approaches.
The main challenge for these techniques is the precise identification of the targets of indirect jumps.~\autocite[4]{angr_2016}

An alternative approach to the problem of code-data separation is to train a supervised model on the machine code, learning from the features found in code and data (we review this family of approaches in~\autoref{ss:statistical-methods}).
\autocite{wartell_2011} proposes a sequential learning model (\emph{Prediction by Partial Matching}) to segment x86 machine code into valid instructions and data.
The model obtains a high accuracy, being able to correctly classify more than 99.8\% of the bytes as code or data.
The model evaluation is done by manually comparing the output of the model with the disassembly from IDA Pro, because precise ground truth for the binaries in the training set is not available. This limitation does not allow to test the method on a large number of binaries.
This approach supports a single architecture (x86), and relies on architecture-specific heuristics: it would need some effort to be adapted for new architectures.


\section*{Proposed Approach}
% [explain in brief what is your proposed approach (summary of and hooks to Section 3)]

In~\autoref{cha:approach}, we propose an approach to solve the aforementioned problems.

To solve the CPU architecture identification problem, we improve the approach described in~\autocite{clemens_2015} by building a Logistic Regression classifier with additional features (i.e., patterns matching the function prologues and epilogues), and by adding Lasso ($L_1$) regularization to make the classifier more robust to noise. We describe our approach in~\autoref{ss:approach-arch-classifier}.

We address the code section identification and the code discovery problems by building a sequential learning model based on Conditional Random Fields (\autoref{sss:model-crf}).
Our model looks at the sequence of bytes in the binary files: for each byte, it learns the probability that it represents code or data, conditioned on its value and on the class labels assigned to itself and to a fixed number of adjacent bytes.
The model, trained on pre-tagged binaries, can identify the code-data boundaries (at the byte level) in unseen samples.
The purpose of the code section identification method (\autoref{ss:approach-code-section}) is to find large, contiguous segments of code inside the file, while the purpose of the code discovery method (\autoref{ss:approach-code-discovery}) is to find the boundaries of each machine-code instruction. For this reason, the code section identification approach also includes a postprocessing phase (\autoref{sss:approach-postprocessing}) to delete small, noisy segments of code or data.

\section*{Implementation}
% [explain in brief what how/if the approach is implemented (summary of and hooks to Section 4)]

In~\autoref{cha:implementation}, we describe the concrete implementation of our approach.
We implemented all our learning methods using Python 3 and the machine learning framework scikit-learn; we implemented the preprocessing and data collection phases which required the usage of external tools with shell scripts.

We implemented the architecture classifier with the \texttt{LogisticRegression} class of scikit-learn (\autoref{sec:impl-archclassifier}). We wrote shell scripts to facilitate the collection and the transformation of some datasets.
We implemented the preprocessing, hyperparameter tuning and model evaluation phases by taking advantage of the rich software library provided by scikit-learn.

To implement the sequential classification models for the code section identification and the code discovery methods, we used \texttt{pystruct}, a structured learning framework for Python, which implements Conditional Random Fields learned with structural SVMs (the mathematical model is described in~\autoref{sec:general-crf}).

We built the preprocessing and the learning phases as Python classes. We designed our classes as wrappers for the general-purpose classification models provided by the libraries. In this way, we expose a simple, uniform interface by hiding the complexity beneath the general-purpose tools.

To train the code discovery model, we need accurate ground truth for the executables in the training set, i.e., the locations of the machine instructions and of the inline data inside the code section. In literature, the majority of the works we found addresses this problem by using existing disassemblers to produce an evaluation baseline; however, this approach may lead to inaccuracies. We choose to extract the ground truth directly from the debugging symbols generated by the compiler, which are more reliable. This approach presents some technical difficulties, which we describe and address in~\autoref{sec:impl-code-discovery}.

\section*{Results Summary}
% [explain what results were obtained in the experimental validation (summary of and hooks to Section 5)]

In~\autoref{cha:experimental-validation}, we describe the evaluation methodology for our approaches, the datasets, the experiments we ran and their results.

To validate our approaches, we use cross-validation and holdout testing, which are standard, well-established techniques in the machine learning domain (\autoref{sec:experimental-setup}).

We extensively evaluate the architecture classifier over multiple datasets of executables; the results show that our method performs better than the state-of-the-art approach~\autocite{clemens_2015} on executables obtained from research papers' datasets and real-world software; it obtains satisfactory performances even on shellcodes and packed binaries, by recognizing the Byte Frequency Distribution of the stub code (\autoref{sec:experiments-arch-classifier}).
Our architecture classifier is robust to noise: the results show that it can correctly identify the architecture of executable files containing both code and data sections.

We evaluate the code section identification method on four datasets, containing binaries of different formats (ELF, PE and Mach-O), of different CPU architectures, compiled with different compilers and multiple levels of optimization (\autoref{sec:experiments-code-section}). The results show that the performance of our model is consistently high: on average, the percentage of correctly classified bytes exceeds 99.5\%.

We evaluate the code discovery method over a dataset of x86 and x86-64 Windows binaries compiled with full debugging symbols to obtain the ground truth.
The results show that our method reaches a high accuracy (over 99.9\%) on this dataset, correctly classifying nearly all the bytes.

We determine the optimal values for the hyperparameters of the models in a systematic way (\autoref{sec:experiments-hyperparam}).

\section*{Original Contributions}
% [conclude with a bullet list that summarizes what are the 3-4 contributions of the thesis over the state of the art]

The main contributions of our work are the following:

\begin{enumerate}
    \item a supervised classification method to identify the Instruction Set Architecture and the endianness of executable files;
    \begin{itemize}
        \item our method is robust to noise: it works on executables containing pure machine code, as well as on samples containing code and data, and packed code;
        \item our method supports, but does not require, architecture-specific signatures: more architectures can be supported by simply extending the training set;
    \end{itemize}
    \item a novel application of a sequential learning model (CRF) to reliably locate the boundaries of the executable regions inside binary files;
    \item a novel, simple and reasonably fast approach, based on a sequential learning model (CRF) to distinguish code from data in stripped and header-less binaries, for two architectures with variable-length instructions (x86, x86-64) and an architecture with fixed-length instructions (ARM);
    \begin{itemize}
        \item an automated technique to generate a training set of Windows executables for the code discovery method, by extracting the ground truth from the debugging symbols generated by the compiler.
    \end{itemize}
\end{enumerate}

\section*{Thesis Organization}

This work is structured in eight chapters.

\begin{itemize}
    \item In~\autoref{cha:introduction}, we give a broad overview about the problems we will be dealing with, the goals of our work, the proposed approach and the results.
    \item In~\autoref{cha:motivation}, we precisely state the definition of the problems, the state-of-the-art solutions to such problems, the goals of our thesis and the challenges we will be facing.
    \item In~\autoref{cha:approach}, we describe in detail the approaches, the algorithms and the models we choose to use to solve the problems stated in~\autoref{cha:motivation}.
    \item In~\autoref{cha:implementation}, we explain how we implemented the approaches described in~\autoref{cha:approach} in a concrete software product.
    \item In~\autoref{cha:experimental-validation}, we evaluate our methods on real data and give an interpretation of the results.
    \item In~\autoref{cha:limitations}, we analyze the limitations of our approach.
    \item In~\autoref{cha:future-works}, we give some suggestions for future research, starting from what we developed in this work.
    \item In~\autoref{cha:conclusions}, we summarize the work done so far and draw the conclusions.
\end{itemize}
